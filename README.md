datalife_office_sequence
========================

The office_sequence module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-office_sequence/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-office_sequence)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
