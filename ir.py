# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.transaction import Transaction

__all__ = ['Sequence', 'SequencePeriod']


class Sequence(metaclass=PoolMeta):
    __name__ = 'ir.sequence'

    def _get_period_pattern(self):
        pattern = super()._get_period_pattern()
        pattern['office'] = (
            Transaction().context.get('office_sequence', None) or
            Transaction().context.get('office', None))
        return pattern


class SequencePeriod(metaclass=PoolMeta):
    __name__ = 'ir.sequence.period'

    office = fields.Many2One('company.office', 'Office', select=True)
